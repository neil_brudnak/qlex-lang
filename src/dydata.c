#include "../include/dydata.h"

List* listCreate(int type){
    List* list = calloc(1,sizeof(List));
    list->total= LIST_INIT_NUM;
    list->current=0;
    list->type = type;
    list->items = calloc(1,sizeof(void*)*LIST_INIT_NUM);
    return list;
}

void listResize(List* list){
    list->total=list->total*2;
    void** items = realloc(list->items, sizeof(void*)*list->total);
    if(items){
        list->items=items;
    }
}
//must send either a pointer to a struct or
//a reference to an int or char
//int a = 1;
//listAppend(list, &a); //correct syntax
void listAppend(List* list, void* item){
    if (list->current >= list->total){
        listResize(list);
    }
    list->items[list->current]=item;
    list->current++;

}
//must insert item in syntax similar to append
//int a = 1;
//listInsert (list, i, &a);
void listInsert(List* list, int index, void* item){
  
   if (index >= list->total || index <0){
    printf("cannot remove index %d, index out of bounds",index);
  }
   list->items[index] = item;
} 

void listRemove(List* list, int index){

  if (index >= list->total || index <0){
    printf("cannot remove index %d, index out of bounds",index);
  }
  void * item; 
  list->items[index] = item;
  list->current-=1;
  for (int i = 0; i<list->current; i++){
    list->items[i] = list->items[i+1];
  }
}

int listGetInt(List* list, int index){
    if (index<=list->current || index>0){
        int i = *(int*) list->items[index];
        return i;
    }
    else {
        printf("index out of bounds\nint list with length %d cannot fit index %d",
                list->current, index);
        exit(1);
    }
}

char listGetChar(List* list, int index){
    if (index>=list->current || index<0){
        char c = *(char*) list->items[index];
        return c;
    }
        
    else {
        printf("index out of bounds\nchar lsitwith length %d cannot fit index %d",
                list->current, index);
        exit(1);
    }
}




