#include "../include/lexer.h"

Lexer* lexerCreate(char* src){
	Lexer* lexer=calloc(1,sizeof(struct LEXER));
	lexer->i = 0;
  lexer->src = lexerStrip(src);
  lexer->cur = lexer->src[lexer->i];
  return lexer;
}

char* lexerGetInput(){

	char *ln = NULL;
	size_t n = 0;
	//ssize_t is posix only so I need to change this eventually
	ssize_t nchr = 0;
/* quick lesson:
 * ssize_t is the unsigned version of size_t
 */
	if ((nchr = getline (&ln, &n, stdin)) != -1){
		//    char exsists and char is not \n and char is not \r then skip char
		while (nchr > 0 && (ln[nchr-1]=='\n' || ln[nchr-1]=='\r' )) ln[--nchr]=0;

		if (!nchr){ //skip blank lines
			free(ln);
			return NULL;
		}

		char* input = strdup (ln);
		free(ln);
		return input;
	}
  	return NULL;
}

char* lexerStrip(char* src){

  //char * ret = malloc(sizeof(src));
  static char ret[sizeof(src)]; 
  int index = 0, i=0;

  while( src[i]!='\0'){

    if (!isspace(src[i])){
      ret[index]=src[i];
      index++;
    }
    i++;
  }
  return ret;
}


void lexerMove(Lexer* lexer){

	lexer->i++;
	lexer->cur=lexer->src[lexer->i];

}
Token* lexerParseTokenId(Lexer* lexer){

    char* id = calloc(1,sizeof(char));
    while ((isalpha(lexer->cur))||(lexer->cur==95)){

      id = realloc(id, (strlen(id)+2) * sizeof(char));
      strcat(id, (char[]){lexer->cur,0});
      lexerMove(lexer);
    }
    Token* tok = lexerTokenCreate(id, _ID);
    return tok;
}

Token* lexerParseIntId(Lexer* lexer){

    char* id = calloc(1,sizeof(char));
    while ((isdigit(lexer->cur))||(lexer->cur==95)){

      id = realloc(id, (strlen(id)+2) * sizeof(char));
      strcat(id, (char[]){lexer->cur,0});
      lexerMove(lexer);
    }
    Token* tok = lexerTokenCreate(id, _INT);
    return tok;
}

Token* lexerParseString(Lexer* lexer){
    lexerMove(lexer);
    char* id = calloc(1,sizeof(char));
    while (lexer->cur!=39){

      id = realloc(id, (strlen(id)+2) * sizeof(char));
      strcat(id, (char[]){lexer->cur,0});
      lexerMove(lexer);
    }
//    lexerMove(lexer);
    Token* tok = lexerTokenCreate(id, _STR);
    return tok;

}

Token* lexerTokenCreate(char* id, int type){

    Token* tok = calloc(1, sizeof(struct TOKEN));
    tok->id = id;
    tok->type = type;
    return tok;
}

Token* lexerGetToken(List* list, int index ){

    if (index<=list->current || index>0){
        Token* tok = (Token*) list->items[index];
        return tok;
    }
    else {
        printf("index out of bounds\nint list with length %d cannot fit index %d",list->current, index);
        exit(1);
    }
}

Token* tokenHandle(char cur, Lexer* lexer){

  Token* tok;

  switch( cur ){

    case '=':
      tok = lexerTokenCreate("=\0", _EQUALS);
      return tok;
    case '(':
      tok = lexerTokenCreate("(\0",_LPAREN);
      return tok;
    case ')':
      tok = lexerTokenCreate(")\0", _RPAREN);
      return tok;
    case '{':
      tok = lexerTokenCreate("{\0", _LBRACKET);
      return tok;
    case '}':
      tok = lexerTokenCreate("}\0", _RBRACKET);
      return tok;
    case '[':
      tok = lexerTokenCreate("[\0", _LBRACE);
      return tok;
    case ']':
      tok = lexerTokenCreate("]\0", _RBRACE);
      return tok;
    case ':':
      tok = lexerTokenCreate(":\0", _COLON);
      return tok;
    case ';':
      tok = lexerTokenCreate(";\0", _SEMI_COLON);
      return tok;
    case ',':
      tok = lexerTokenCreate(",\0", _COMMA);
      return tok;
    case '+':
      tok = lexerTokenCreate("+\0", _PLUS);
      return tok;
    case '-':
      tok = lexerTokenCreate("-\0", _MINUS);
      return tok;
    case '/':
      tok = lexerTokenCreate("/\0", _DIV);
      return tok;
    case '*':
      tok = lexerTokenCreate("*\0", _STAR);
      return tok;
    case '>':
      tok = lexerTokenCreate(">\0", _GT);
      return tok;
    case '<':
      tok = lexerTokenCreate("<\0", _LT);
      return tok;
    case'?':
      tok = lexerTokenCreate("?\0", _QST);
      return tok;
    case'&':
      tok = lexerTokenCreate("&\0",_REF);
      return tok;
    //39 is ' charecter 
    case 39:
      tok = lexerParseString(lexer);
      return tok;
    default:
      printf("token %c: %d not expected\n", cur, cur);
      exit(1);

    }

}
void lexerMainLoop(Lexer* lexer, List* tokenList){

  printf("%s\n", lexer->src);
  while (lexer->i < strlen(lexer->src)){
    if (isdigit(lexer->cur)){
      Token* tok = lexerParseIntId(lexer);
      listAppend(tokenList, tok);
    }
    if (isalpha(lexer->cur)){

      Token* tok = lexerParseTokenId(lexer);
      listAppend(tokenList, tok);
    } 
    else{
      Token* tok = tokenHandle(lexer->cur, lexer);
      listAppend(tokenList, tok);
      lexerMove(lexer);
    }
    
}
  
  int i;
  for (i = 0; i < tokenList->current; i++){
    Token* tok = lexerGetToken(tokenList,i);
    printf("Token: id='%s' index=%d type=%d\n",tok->id, i, tok->type);
      
  }
    
  
  
}

