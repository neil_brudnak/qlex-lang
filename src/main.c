#include <stdio.h>
#include "../include/parser.h"
#include "../include/lexer.h"
#include "../include/dydata.h"
void main(int argc, char* argv[]){
  
  List* tokenList = listCreate(Tok);
  //char* input = lexerGetInput();
  Lexer* lexer = lexerCreate(argv[1]);
  lexerMainLoop(lexer,tokenList);
  List* astList = listCreate(Tree);
  parseTokenList(tokenList,astList);
  astTree* node = parserGetAstTree(astList,0);
  //parserPrintTree(node);
}

