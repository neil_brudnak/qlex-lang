#include "../include/parser.h"

astTree* parserEvalToken(Token* tok) {  
  //allocate memory for tree
  astTree* tree = calloc(1,sizeof(struct AST_TREE));
  char* tokId = tok->id;
  tree->id = tokId;
  //capture all operators
  if (tok->type <7){

    tree->type = astOperator;
    tree->op = tok->type;
    tree->id=tok->id;
    return tree;
  }
  else{
    switch (tok->type){
      //capture all ints
      case _INT:
        tree->type = astInt;
        tree->intValue = atoi(tok->id);
        return tree;
      //capture all strings
      case _STR:
        tree->type = astString;
        return tree;
      //capture all patterns
      case _QST:
        tree->type = astPattern;
        return tree;
      //capture all ids
      case _ID:
        tree->type = astId;
        return tree;
      //capture all terminators
      case _SEMI_COLON:
        tree->type = astTerminator;
        return tree;
      case _LPAREN:
        tree->type = astFunction;
        return tree;
      case _RPAREN:
        tree->type = astTerminator;
        return tree;
      case _LBRACE:
        tree->type = astExpression;
        return tree;
      case _RBRACE:
        tree->type = astTerminator;
        return tree;
      default:
        return tree;
      }
  }
    
}
int parserPeekToken(List* tokenList, int index, int offset){

  int i = index+offset, retType;
  Token* tok = lexerGetToken(tokenList, i);
  retType = tok->type;
  //free(tok);
  return retType;
}
astTree* parserParseExpression(List* tokenList,int index){

  if(parserPeekToken(tokenList, index,1)==_ID &&
      parserPeekToken(tokenList, index,2)==_LPAREN){
  }

  //allocate memory for root of tree and expression being returned
  astTree* E = calloc(1,sizeof(struct AST_TREE));
  //helper ast for which to hold the current ast being worked on
  astTree* curE = calloc(1,sizeof(struct AST_TREE));
  E->type = astExpression;
  //initialize current expression with base expression
  curE = E;
  while(1){
    //setup current node being analyzed
    index++;
    Token* tok = lexerGetToken(tokenList, index);
    astTree* tree = parserEvalToken(tok);
    //end if terminator is found
    if (tree->type==astTerminator){
      break;
    }

    //printf("%s---%d\n",tree->id, tree->type);
    //evaluate left side
    if (curE->left == NULL){
      printf("in left\n");
      curE->left = tree;
      //printf("%s\n",curE->left->id);
      continue;
    }
    //evaluate middle 
    else if (curE->middle == NULL){
      printf("in middle\n");
      curE->middle = tree;
      printf("%s\n",curE->middle->id);
      continue;
    }
    //evaluate right
    else if (curE->right == NULL){
      printf("in right\n");
      curE->right = tree;
      printf("%s\n",curE->right->id);
      continue;
    }
    //evalue ate right side if expression depth > 1
    else if (curE->right != NULL){
      //printf("creating new expression node\n");
      //allocate memory for new expression node to be worked on
      astTree* E2 = calloc(1,sizeof(struct AST_TREE));
      E2->type = astExpression;
      E2->id = "E";
      E2->left = curE->right;
      E2->middle = tree;
      curE->right = E2;
      //change current node to correct depth
      curE=E2;
    }
    
  }
  //return the base node for full expression
  return E;
}
Function* parserParseFunction(List* tokenList, int index){
  
  int argCount = 0;
  //index++;
  Token* tok = lexerGetToken(tokenList, index);
  Function* func = calloc(1,sizeof(struct FUNCTION));
  index++;
  while (tok->type!=_RPAREN && 
      parserPeekToken(tokenList, index, 1)!=_SEMI_COLON){
    index++;
    Token* tok = lexerGetToken(tokenList, index);
    if(tok->type != _COMMA){
      func->args[argCount]=tok->id;
      argCount++;
    } 
  }
  index++;
  return func;
}


void parseTokenList(List* tokenList, List* astList){
  int index = 0, cap = tokenList->current;
  while(index<tokenList->current){
    Token* tok = lexerGetToken(tokenList,index);
    astTree* tree = parserEvalToken(tok);
    switch(tree->type){

      case astId:
        index++;
        Token* tok = lexerGetToken(tokenList,index);
        astTree* node = parserEvalToken(tok);
        printf("%s\n",node->id);
        if (node->type == astOperator && node->op == _EQUALS){
          astTree* e = parserParseExpression(tokenList, index);
          e->type = astAsign;
          e->id = node->id;
          listAppend(astList, e);

        }
        else if (node->type == astFunction){
          Function* func = parserParseFunction(tokenList, index);
          func->id = node->id;
          printf("%s\n",func->id);
        }
        break;
      default:
        break;
    }
    index++;
  }
  
}
astTree* parserGetAstTree(List* list, int index ){

    if (index<=list->current || index>0){
        astTree* tree = (astTree*) list->items[index];
        return tree;
    }
    else {
        printf("index out of bounds\nint list with length %d cannot fit index %d",list->current, index);
        exit(1);
    }
}


void parserPrintTree(astTree* node){

  astTree* head = malloc(sizeof(node));
  head = node;
  printf("       E       \n");
  while (head->left){
    printf(" /     |      %c \n", 92);
    printf("%s     %s     %s \n",head->left->id, 
                                 head->middle->id, 
                                 head->right->id);
    head = head->right;

  }

}

