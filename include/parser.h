#ifndef QLEX_PARSER_H
#define QLEX_PARSER_H

#include <string.h>
#include <stdlib.h>
#include "lexer.h"



typedef struct AST_TREE{

  char* id;
  enum{
    astExpression,
    astInt,
    astString,
    astPattern,
    astOperator,
    astAsign,
    astId,
    astTerminator,
    astFunction,
  }type;
  int op;
  int intValue;


  struct AST_TREE* middle;
  struct AST_TREE* left;
  struct AST_TREE* right;
}astTree;
typedef struct FUNCTION{

  enum{
    DEFAULT,
    FIND,
    REPLACE,
    _FILE,
    _LINE,
  }type;
  char* id;
  const char* args[5];
}Function;

void parseTokenList(List* tokenList, List* astList);
int parserPeekToken(List* tokenList, int index, int offest);
astTree* parserEvalToken(Token* tok);
astTree* parserParseExpression(List* tokenList, int index);
void parserPrintTree(astTree* node);
astTree* parserGetAstTree(List* list, int index);
astTree* parserParserFunction(List* tokenList,int index); 
#endif








