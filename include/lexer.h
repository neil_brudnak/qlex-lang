#ifndef QLEX_LEXER_H
#define QLEX_LEXER_H

#include "dydata.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

typedef struct LEXER{
  
  char* src;
  unsigned int i;
  char cur;
}Lexer;

typedef struct TOKEN{

    char* id;

    enum{

        _EQUALS,
        _PLUS,      //15
        _MINUS,     //16
        _DIV,       //17
        _STAR,       //18
        _GT,        //22
        _LT, 
        _ID,
        _INT,        //0
        _STR,
        _QST,
        _REF,
        _LPAREN,    //2
        _RPAREN,    //3
        _LBRACE,    //4
        _RBRACE,    //5
        _LBRACKET,  //6
        _RBRACKET,  //7
        _COLON,     //8
        _SEMI_COLON,//9
        _COMMA,     //10
               //23
    }type;

  }Token;

Lexer* lexerCreate();
char* lexerGetInput();
void lexerMove(Lexer* lexer);
char* lexerStrip(char* src);
Token* lexerParseTokenId(Lexer* lexer);
Token* lexerParseIntId(Lexer* lexer);
Token* lexerParseString(Lexer* lexer);
Token* lexerTokenCreate(char* id, int type);
Token* lexerGetToken(List* list, int index);
Token* lexerTokenHandle(char cur, Lexer* lexer);
void lexerMainLoop(Lexer* lexer, List* tokenList);
#endif
                   
   
