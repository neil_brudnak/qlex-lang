#ifndef QLEX_DYDATA_H
#define QLEX_DYDATA_H

#include <stdio.h>
#include <stdlib.h>

#define LIST_INIT_NUM 4

typedef struct{
    size_t total;
    int current;
    void** items;
    enum{
        Int,
        Char,
        Tok,
        Tree,
    }type;
}List;

List* listCreate(int type);
void listResize(List* list);
void listAppend(List* list, void* item);
void listInsert(List* list, int index, void* item);
void listRemove(List* list, int index);
int listGetInt(List* list, int index);
char listGetChar(List* list, int index);  
#endif


